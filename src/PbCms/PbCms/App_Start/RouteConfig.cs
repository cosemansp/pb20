﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PbCms
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("users-login", "account/login", new { controller = "Authentication", action = "Login" }, new[] { "PbCms.Controllers" });
            routes.MapRoute("users-logout", "account/logout", new { controller = "Authentication", action = "Logout" }, new[] { "PbCms.Controllers" });
            routes.MapRoute("users-forgotpassword", "account/forgotpassword", new { controller = "Authentication", action = "ForgotPassword" }, new[] { "PbCms.Controllers" });
            routes.MapRoute("users-resetpassword", "account/resetpassword", new { controller = "Authentication", action = "ResetPassword" }, new[] { "PbCms.Controllers" });
            routes.MapRoute("users-register", "account/signup/{key}", new { controller = "Authentication", action = "Register", key = "" }, new[] { "PbCms.Controllers" });

        }
    }
}