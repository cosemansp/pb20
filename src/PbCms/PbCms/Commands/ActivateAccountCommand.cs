using System.Linq;
using BetterCms.Core.Mvc.Commands;
using BetterCms.Module.Root.Mvc;
using BetterCms.Module.Users.Models;
using PbCms.Models;

namespace PbCms.Commands
{
    public class ActivateAccountCommand : CommandBase, ICommand<ActivationViewModel, bool>
    {
        public bool Execute(ActivationViewModel request)
        {
            var user = Repository.AsQueryable<User>().FirstOrDefault(x => x.Email == request.Email);
            if (user != null)
            {
                if (user.FirstName == request.Key)
                {
                    user.FirstName = "_activated_";
                    Repository.Save(user);
                    UnitOfWork.Commit();
                    return true;
                }
            }
            return false;
        }
    }
}