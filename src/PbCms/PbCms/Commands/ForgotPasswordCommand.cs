﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using BetterCms.Core.Mvc.Commands;
using BetterCms.Module.Root.Mvc;
using BetterCms.Module.Users.Models;
using PbCms.Models;

namespace PbCms.Commands
{
    public class ForgotPasswordCommand : CommandBase, ICommand<ForgotPasswordViewModel, bool>
    {
        public bool Execute(ForgotPasswordViewModel request)
        {
            try
            {
                var user = Repository.AsQueryable<User>().FirstOrDefault(x => x.Email == request.Email);
                if (user == null)
                    return false;

                user.FirstName = "rp-" + Guid.NewGuid();
                Repository.Save(user);

                // send email with password link
                var mailMsg = new MailMessage("info@projectbaselinebelgium.be", user.Email);
                mailMsg.Subject = "Password reset from Projectbaseline Belgium";

                var siteRoot = ConfigurationManager.AppSettings["app.siteroot"];
                var resetLink = string.Format("http://{0}/account/resetpassword/?key={1}", siteRoot, user.FirstName);

                string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/PasswordReset.html"));
                text = text.Replace("$Name", user.LastName);
                text = text.Replace("$PasswordResetLink", resetLink);
                mailMsg.Body = text;
                mailMsg.IsBodyHtml = true;

                var mailClient = new SmtpClient();
                mailClient.Send(mailMsg);

                UnitOfWork.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
    }
}