﻿using System;
using System.Web;
using System.Web.Security;
using BetterCms.Core.Exceptions;
using BetterCms.Core.Exceptions.Mvc;
using BetterCms.Core.Mvc.Commands;
using BetterCms.Module.Root.Mvc;
using BetterCms.Module.Users.Content.Resources;
using PbCms.Models;

namespace PbCms.Commands
{
    public class LoginCommand : CommandBase, ICommand<LoginViewModel, HttpCookie>
    {
        public HttpCookie Execute(LoginViewModel request)
        {
            var userName = Membership.GetUserNameByEmail(request.Email);
            if (!Membership.ValidateUser(userName, request.Password))
                throw new ValidationException(() => UsersGlobalization.Login_UserNameOrPassword_Invalid, "User name or password is invalid.");

            if (!Roles.Enabled)
                throw new CmsException("A roles provider should be enabled in web.config.");

            var ticket = new FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMonths(1), request.RememberMe, string.Empty);
            return new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
            {
                Expires = ticket.Expiration,
                Path = FormsAuthentication.FormsCookiePath
            };
        }
    }
}