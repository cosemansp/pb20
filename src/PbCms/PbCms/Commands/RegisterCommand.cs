using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using BetterCms.Core.Exceptions.Mvc;
using BetterCms.Core.Mvc.Commands;
using BetterCms.Module.Root.Mvc;
using BetterCms.Module.Users.Models;
using BetterCms.Module.Users.Services;
using PbCms.Models;

namespace PbCms.Commands
{
    public class RegisterCommand : CommandBase, ICommand<RegisterViewModel, bool>
    {
        private readonly IAuthenticationService _authenticationService;

        public RegisterCommand(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public bool Execute(RegisterViewModel request)
        {
            // validate password
            if (request.Password != request.PasswordVerify)
                throw new ValidationException(() => "Password confirmation doesn't match password.", "");

            // validate unique username
            var userName = (request.FirstName + request.LastName).Replace(" ", "").ToLower();
            var otherUserWithUserName = Repository.AsQueryable<User>().FirstOrDefault(x => x.UserName == userName);
            if (otherUserWithUserName != null)
                throw new ValidationException(() => "An account with this name already exist.", "");

            // validate unique email
            var otherUserWithEmail = Repository.AsQueryable<User>().FirstOrDefault(x => x.Email == request.Email);
            if (otherUserWithEmail != null)
                throw new ValidationException(() => "An account with this name already exist.", "");

            // all ok, create user
            var user = new User();
            string passwordSaltBase64 = _authenticationService.GeneratePasswordSalt();
            List<Role> list = Repository.AsQueryable<Role>().Where(f => !f.IsSystematic).ToList();  // all non system roles
            user.FirstName = "ac-" + Guid.NewGuid();
            user.LastName = request.FirstName + " " + request.LastName;
            user.Email = request.Email;
            user.UserName = userName;
            user.Password = _authenticationService.CreatePasswordHash(request.Password, passwordSaltBase64);
            user.Salt = passwordSaltBase64;
            user.UserRoles = list.Select(role => new UserRole
            {
                User = user,
                Role = role
            }).ToList();
            Repository.Save(user);

            // send email with password link
            var siteRoot = ConfigurationManager.AppSettings["app.siteroot"];
            var registrationLink = string.Format("http://{0}/account/signup/{1}?email={2}", siteRoot, user.FirstName, user.Email);
            var mailMsg = new MailMessage("info@projectbaselinebelgium.be", user.Email);
            mailMsg.Subject = "Account activation Projectbaseline Belgium";
            string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"~/RegistrationConfirmation.html"));
            text = text.Replace("$Name", user.LastName);
            text = text.Replace("$RegistrationLink", registrationLink);
            mailMsg.Body = text;
            mailMsg.IsBodyHtml = true;
            var mailClient = new SmtpClient();
            mailClient.Send(mailMsg);

            UnitOfWork.Commit();
            return true;
        }
    }
}