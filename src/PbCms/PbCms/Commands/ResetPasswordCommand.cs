using System.Linq;
using BetterCms.Core.Exceptions.Mvc;
using BetterCms.Core.Mvc.Commands;
using BetterCms.Module.Root.Mvc;
using BetterCms.Module.Users.Models;
using BetterCms.Module.Users.Services;
using PbCms.Models;

namespace PbCms.Commands
{
    public class ResetPasswordCommand : CommandBase, ICommand<ResetPasswordViewModel, bool>
    {
        private readonly IAuthenticationService _authenticationService;

        public ResetPasswordCommand(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public bool Execute(ResetPasswordViewModel request)
        {
            var user = Repository.AsQueryable<User>().FirstOrDefault(x => x.Email == request.Email);
            if (user == null || user.FirstName != request.Key)
                throw new ValidationException(() => "Invalid password reset or unknown account", "");

            if (request.Password != request.PasswordVerify)
                throw new ValidationException(() => "Password confirmation doesn't match password.", "");


            string passwordSaltBase64 = _authenticationService.GeneratePasswordSalt();
            user.FirstName = "_activated_";
            user.Password = _authenticationService.CreatePasswordHash(request.Password, passwordSaltBase64);
            user.Salt = passwordSaltBase64;
            Repository.Save(user);
            UnitOfWork.Commit();
            return true;
        }
    }
}