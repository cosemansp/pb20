﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using BetterCms.Module.Root.Mvc;
using Common.Logging;
using MvcContrib.UI.InputBuilder.InputSpecification;
using PbCms.Commands;
using PbCms.Models;

namespace PbCms.Controllers
{
    public class AuthenticationController : CmsControllerBase
    {
        private static readonly ILog Log = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect(FormsAuthentication.DefaultUrl ?? "/");

            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View(new ForgotPasswordViewModel());
        }

        [HttpGet]
        public ActionResult Register(ActivationViewModel model)
        {
            if (string.IsNullOrEmpty(model.Key))
                return View(new RegisterViewModel());

            var activateAccountCommand = GetCommand<ActivateAccountCommand>();
            if (activateAccountCommand.ExecuteCommand(model))
                return View("Login", new LoginViewModel {Activated = true});

            return View(new RegisterViewModel { Activated = true, ActivatedFailed = true});
        }

        [HttpGet]
        public ActionResult ResetPassword(string key)
        {
            return View(new ResetPasswordViewModel { Key = key });
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var cookie = GetCommand<LoginCommand>().ExecuteCommand(model);
                if (cookie != null)
                {
                    Response.Cookies.Add(cookie);
                    return Redirect(model.ReturnUrl ?? FormsAuthentication.DefaultUrl ?? "/");
                }
            }

            return View(new LoginViewModel { Failed = true });
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            var forgotPasswordCommand = GetCommand<ForgotPasswordCommand>();
            if (forgotPasswordCommand.ExecuteCommand(model))
            {
                return View(new ForgotPasswordViewModel { MailIsSent = true});
            }
     
            if (forgotPasswordCommand.Context.Messages.Error.Count > 0 )
                return View(new ForgotPasswordViewModel { Failure = true });

            return View(new ForgotPasswordViewModel { InvalidEmail = true });
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            var command = GetCommand<RegisterCommand>();
            command.ExecuteCommand(model);

            if (command.Context.Messages.Error.Count > 0)
            {
                model.ErrorMessage = command.Context.Messages.Error.FirstOrDefault();
                model.IsFailed = true;
            }
            else
            {
                model.Registered = true;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {

            var command = GetCommand<ResetPasswordCommand>();
            command.ExecuteCommand(model);

            if (command.Context.Messages.Error.Count > 0)
            {
                model.ErrorMessage = command.Context.Messages.Error.FirstOrDefault();
                model.IsFailed = true;
            }
            else
            {
                model.PasswordResetted = true;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            try
            {
                return SignOutUserIfAuthenticated();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Failed to logout user {0}.", ex, new object[1] { User.Identity });
            }
            return Redirect(FormsAuthentication.LoginUrl);
        }


    }
}