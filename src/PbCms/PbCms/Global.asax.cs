﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Core;
using BetterCms.Core;
using BetterCms.Core.Dependencies;
using BetterCms.Core.Environment.Host;
using BetterCms.Core.Mvc.Commands;
using PbCms.Commands;
using PbCms.Controllers;
using PbCms.Models;

namespace PbCms
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static ICmsHost _cmsHost;

        protected void Application_Start()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<AuthenticationController>().AsSelf().PropertiesAutowired();
            containerBuilder.RegisterType<LoginCommand>().AsSelf();
            containerBuilder.RegisterType<RegisterCommand>().AsSelf().PropertiesAutowired();
            containerBuilder.RegisterType<ForgotPasswordCommand>().AsSelf().PropertiesAutowired();
            containerBuilder.RegisterType<ActivateAccountCommand>().AsSelf().PropertiesAutowired();
            containerBuilder.RegisterType<ResetPasswordCommand>().AsSelf().PropertiesAutowired();

            ContextScopeProvider.RegisterTypes(containerBuilder);

            _cmsHost = CmsContext.RegisterHost();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            _cmsHost.OnApplicationStart(this);
        }

        protected void Application_BeginRequest()
        {
            // [YOUR CODE]

            _cmsHost.OnBeginRequest(this);
        }

        protected void Application_EndRequest()
        {
            // [YOUR CODE]

            _cmsHost.OnEndRequest(this);
        }

        protected void Application_Error()
        {
            // [YOUR CODE]

            _cmsHost.OnApplicationError(this);
        }

        protected void Application_End()
        {
            // [YOUR CODE]

            _cmsHost.OnApplicationEnd(this);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            // [YOUR CODE]

            // Uncomment following source code for a quick Better CMS test if you don't have implemented users authentication. 
            // Do not use this code for production!
            /*
            var roles = new[] { "BcmsEditContent", "BcmsPublishContent", "BcmsDeleteContent", "BcmsAdministration" };
            var principal = new GenericPrincipal(new GenericIdentity("TestUser"), roles);
            HttpContext.Current.User = principal;
            */

            _cmsHost.OnAuthenticateRequest(this);
        }
    }
}