﻿namespace PbCms.Models
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
        public bool MailIsSent { get; set; }
        public bool Failure { get; set; }
        public bool InvalidEmail { get; set; }
    }
}