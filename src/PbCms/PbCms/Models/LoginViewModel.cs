﻿namespace PbCms.Models
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
        public bool Failed { get; set; }
        public bool Activated { get; set; }

        public override string ToString()
        {
            return string.Format("UserName: {0}", Email);
        }
    }
}