﻿namespace PbCms.Models
{
    public class ActivationViewModel
    {
        public string Key { get; set; }
        public string Email { get; set; }
    }

    public class RegisterViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordVerify { get; set; }

        public string ErrorMessage { get; set; }
        public bool IsFailed { get; set; }
        public bool Registered { get; set; }
        public bool Activated { get; set; }
        public bool ActivatedFailed { get; set; }
    }
}