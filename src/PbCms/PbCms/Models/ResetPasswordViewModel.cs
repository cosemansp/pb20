﻿namespace PbCms.Models
{
    public class ResetPasswordViewModel
    {
        public string Email { get; set; }
        public string Key { get; set; }
        public string Password { get; set; }
        public string PasswordVerify { get; set; }

        public bool PasswordResetted { get; set; }
        public bool IsFailed { get; set; }
        public string ErrorMessage { get; set; }
    }
}