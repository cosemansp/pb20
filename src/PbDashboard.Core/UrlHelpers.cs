﻿using System;
using System.Web.Mvc;
using Umbraco.Web;

namespace PbDashboard.Core
{
    public static class UrlHelpers
    {
        public static string GetNodeUrl(this UrlHelper urlHelper, string alias)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var contentNode = umbracoHelper.TypedContentSingleAtXPath(String.Format("//*[@nodeName= '{0}']", alias));
            return contentNode.Url;
        }
    }
}