﻿using System.Linq;
using System.Web.Mvc;

namespace PbDashboard.Core
{
    public static class WebViewPageExtensions
    {
        public static void MapModel<T>(this WebViewPage<T> page) where T : class
        {
            var models = page.ViewContext.TempData.Where(item => item.Value is T).ToList();
            if (models.Any())
            {
                page.ViewData.Model = (T)models.First().Value;
                page.ViewContext.TempData.Remove(models.First().Key);
            }
        }
    }
}