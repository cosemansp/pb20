﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Umbraco.Web.Editors;
using Umbraco.Web.Trees;

namespace PbDashboard
{
    public class BootStrapper
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            //register all controllers found in this assembly
            builder.RegisterControllers(typeof(Global).Assembly);
            builder.RegisterControllers(typeof(BackOfficeController).Assembly);
            builder.RegisterApiControllers(typeof(ContentTreeController).Assembly);

            //add custom class to the container as Transient instance
            //builder.RegisterType<MyAwesomeContext>();

            var container = builder.Build();

            // mvc
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // webapi
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        } 
    }
}