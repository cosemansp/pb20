﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace PbDashboard
{
    public class Global : Umbraco.Web.UmbracoApplication
    {
        protected override void OnApplicationStarted(object sender, EventArgs e)
        {
            base.OnApplicationStarted(sender, e);

            BootStrapper.Configure();
        }
    }

    public class AutofacDependencyResolverEx : IDependencyResolver
    {
        private readonly ILifetimeScope _container;
        private readonly Action<ContainerBuilder> _configurationAction;
        private ILifetimeScopeProvider _lifetimeScopeProvider;

        /// <summary>
        /// Gets the Autofac implementation of the dependency resolver.
        /// 
        /// </summary>
        public static AutofacDependencyResolverEx Current
        {
            get
            {
                return DependencyResolver.Current.GetService<AutofacDependencyResolverEx>();
            }
        }

        /// <summary>
        /// The lifetime containing components for processing the current HTTP request.
        /// 
        /// </summary>
        public ILifetimeScope RequestLifetimeScope
        {
            get
            {
                var configurationAction = (Action<ContainerBuilder>)(builder =>
                {
                    if (this._configurationAction != null)
                        this._configurationAction(builder);
                    builder.RegisterInstance(this).As<AutofacDependencyResolverEx>();
                });
                if (this._lifetimeScopeProvider == null)
                    this._lifetimeScopeProvider = new RequestLifetimeScopeProvider(this._container);
                return this._lifetimeScopeProvider.GetLifetimeScope(configurationAction);
            }
        }

        /// <summary>
        /// Gets the application container that was provided to the constructor.
        /// 
        /// </summary>
        public ILifetimeScope ApplicationContainer
        {
            get
            {
                return this._container;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Autofac.Integration.Mvc.AutofacDependencyResolver"/> class.
        /// 
        /// </summary>
        /// <param name="container">The container that nested lifetime scopes will be create from.</param>
        public AutofacDependencyResolverEx(ILifetimeScope container)
        {
            if (container == null)
                throw new ArgumentNullException("container");
            this._container = container;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Autofac.Integration.Mvc.AutofacDependencyResolver"/> class.
        /// 
        /// </summary>
        /// <param name="container">The container that nested lifetime scopes will be create from.</param><param name="configurationAction">Action on a <see cref="T:Autofac.ContainerBuilder"/>
        ///             that adds component registations visible only in nested lifetime scopes.</param>
        public AutofacDependencyResolverEx(ILifetimeScope container, Action<ContainerBuilder> configurationAction)
            : this(container)
        {
            if (configurationAction == null)
                throw new ArgumentNullException("configurationAction");
            this._configurationAction = configurationAction;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Autofac.Integration.Mvc.AutofacDependencyResolver"/> class.
        /// 
        /// </summary>
        /// <param name="container">The container that nested lifetime scopes will be create from.</param><param name="lifetimeScopeProvider">A <see cref="T:Autofac.Integration.Mvc.ILifetimeScopeProvider"/> implementation for
        ///             creating new lifetime scopes.</param>
        public AutofacDependencyResolverEx(ILifetimeScope container, ILifetimeScopeProvider lifetimeScopeProvider)
            : this(container)
        {
            if (lifetimeScopeProvider == null)
                throw new ArgumentNullException("lifetimeScopeProvider");
            this._lifetimeScopeProvider = lifetimeScopeProvider;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Autofac.Integration.Mvc.AutofacDependencyResolver"/> class.
        /// 
        /// </summary>
        /// <param name="container">The container that nested lifetime scopes will be create from.</param><param name="lifetimeScopeProvider">A <see cref="T:Autofac.Integration.Mvc.ILifetimeScopeProvider"/> implementation for
        ///             creating new lifetime scopes.</param><param name="configurationAction">Action on a <see cref="T:Autofac.ContainerBuilder"/>
        ///             that adds component registations visible only in nested lifetime scopes.</param>
        public AutofacDependencyResolverEx(ILifetimeScope container, ILifetimeScopeProvider lifetimeScopeProvider, Action<ContainerBuilder> configurationAction)
            : this(container, lifetimeScopeProvider)
        {
            if (configurationAction == null)
                throw new ArgumentNullException("configurationAction");
            this._configurationAction = configurationAction;
        }

        /// <summary>
        /// Get a single instance of a service.
        /// 
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns>
        /// The single instance if resolved; otherwise, <c>null</c>.
        /// </returns>
        public object GetService(Type serviceType)
        {
            var obj = RequestLifetimeScope.ResolveOptional(serviceType);
            return obj;
        }

        /// <summary>
        /// Gets all available instances of a services.
        /// 
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns>
        /// The list of instances if any were resolved; otherwise, an empty list.
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return (IEnumerable<object>)RequestLifetimeScope.Resolve(typeof(IEnumerable<>).MakeGenericType(new Type[1]
            {
                serviceType
            }));
        }
    }
}