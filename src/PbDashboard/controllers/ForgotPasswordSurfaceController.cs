﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Mvc;
using PbDashboard.Core;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace PbDashboard.controllers
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
        public bool MailIsSent { get; set; }
        public bool Failure { get; set; }
        public bool InvalidEmail { get; set; }
    }

    public class ForgotPasswordSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult ForgotPassword()
        {
            return PartialView("_forgotPassword", new ForgotPasswordViewModel());
        }

        [HttpPost]
        public ActionResult PostForgetPassword(ForgotPasswordViewModel model)
        {
            var member = Services.MemberService.GetByEmail(model.Email);
            if (member == null)
            {
                model.InvalidEmail = true;
                TempData.Add("model", model);
                return CurrentUmbracoPage();
            }

            try
            {
                // send email with password link
                var activationKey = "rp-" + Guid.NewGuid();
                var siteRoot = ConfigurationManager.AppSettings["app.sitedomain"];
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var contentNode = umbracoHelper.TypedContentSingleAtXPath("ResetPassword");
                var resetPasswordPath = contentNode.Url.TrimEnd("/".ToCharArray());
                var mailMsg = new MailMessage("info@projectbaselinebelgium.be", member.Email);
                mailMsg.Subject = "Password reset from Projectbaseline Belgium";
                var resetLink = string.Format("http://{0}{1}?key={2}", siteRoot, resetPasswordPath, activationKey);
                string text =
                    System.IO.File.ReadAllText(HttpContext.Server.MapPath(@"~/App_Data/Emails/PasswordReset.html"));
                text = text.Replace("$Name", member.Name);
                text = text.Replace("$PasswordResetLink", resetLink);
                mailMsg.Body = text;
                mailMsg.IsBodyHtml = true;

                var mailClient = new SmtpClient();
                mailClient.Send(mailMsg);

                // save member
                member.SetValue("activationKey", activationKey);
                Services.MemberService.Save(member);

                model.MailIsSent = true;
                TempData.Add("model", model);
                return CurrentUmbracoPage();
            }
            catch
            {
                model.Failure = true;
                TempData.Add("model", model);
                return CurrentUmbracoPage();
            }
        }
    }
}