﻿using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace PbDashboard.controllers
{
    public class ActivationViewModel
    {
        public string Key { get; set; }
        public string Email { get; set; }
    }

    public class LoginViewModel 
    {
        public bool Failed { get; set; }
        public bool Activated { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class LoginSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Login()
        {
            return PartialView("_login", new LoginViewModel());
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect("/");
        }

        [HttpPost]
        public ActionResult PostLogin(LoginViewModel model)
        {
            var member = Services.MemberService.GetByEmail(model.Email);
            if (member != null && Membership.ValidateUser(member.Username, model.Password))
            {
                FormsAuthentication.SetAuthCookie(member.Username, model.RememberMe);
                return RedirectToCurrentUmbracoPage();
            }
            model.Failed = true;
            TempData.Add("model", model);
            return CurrentUmbracoPage();
        }
    }
}