﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Net.Mail;
using System.Web.ModelBinding;
using System.Web.Mvc;
using PbDashboard.Core;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace PbDashboard.controllers
{
    public class RegisterViewModel
    {
        public string Name { get; set; }
        public string Cert { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordVerify { get; set; }

        public string ErrorMessage { get; set; }
        public bool IsFailed { get; set; }
        public bool Registered { get; set; }
        public bool Activated { get; set; }
        public bool ActivatedFailed { get; set; }
    }

    public class RegisterSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Register(string key, string email)
        {
            var model = new RegisterViewModel();
            if (!string.IsNullOrEmpty(key))
            {
                var member = Services.MemberService.GetByEmail(email);
                if (member != null)
                {
                    if (member.GetValue<string>("activationKey") == key)
                    {
                        member.IsApproved = true;
                        member.SetValue("activationKey", "");
                        Services.MemberService.Save(member);
                        model.Activated = true;
                    }
                }
                else
                {
                    model.ActivatedFailed = true;
                }
            }
            return PartialView("_register", model);
        }

        [HttpPost]
        public ActionResult PostRegister(RegisterViewModel model)
        {
            // validation
            var userName = (model.Name).Replace(" ", "").ToLower();
            try
            {
                if (Services.MemberService.GetByUsername(userName) != null)
                    throw new ValidationException("An account with this name already exist.");

                if (Services.MemberService.GetByEmail(model.Email) != null)
                    throw new ValidationException("An account with this email already exist.");

                if (model.Password != model.PasswordVerify)
                    throw new ValidationException("Password confirmation doesn't match password.");
            
                // create user
                var member = Services.MemberService.CreateMember(userName, model.Email, model.Name, "member");
                member.IsApproved = false;
                Services.MemberService.SavePassword(member, model.Password);

                // send email with confirmation link
                var activationKey = "ac-" + Guid.NewGuid();
                var siteRoot = ConfigurationManager.AppSettings["app.sitedomain"];
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var contentNode = umbracoHelper.TypedContentSingleAtXPath("Register");
                var registerPath = contentNode.Url.TrimEnd("/".ToCharArray());

                var registrationLink = string.Format("http://{0}{1}?key={2}&email={3}", siteRoot, registerPath ,activationKey, member.Email);
                var mailMsg = new MailMessage("info@projectbaselinebelgium.be", member.Email);
                mailMsg.Subject = "Account activation Projectbaseline Belgium";
                string text = System.IO.File.ReadAllText(HttpContext.Server.MapPath(@"~/App_Data/Emails/RegistrationConfirmation.html"));
                text = text.Replace("$Name", member.Name);
                text = text.Replace("$RegistrationLink", registrationLink);
                mailMsg.Body = text;
                mailMsg.IsBodyHtml = true;
                var mailClient = new SmtpClient();
                mailClient.Send(mailMsg);

                member.SetValue("activationKey", activationKey);
                Services.MemberService.Save(member);
            }
            catch (ValidationException ex)
            {
                model.ErrorMessage = ex.Message;
                model.IsFailed = true;
                TempData.Add("model", model);
                return CurrentUmbracoPage();
            }
            catch (Exception ex)
            {
                model.ErrorMessage = "Something went wrong, this should not happen. Please try again.";
                model.IsFailed = true;
                TempData.Add("model", model);
                return CurrentUmbracoPage();
            }

            model.Registered = true;
            TempData.Add("model", model);
            return CurrentUmbracoPage();
        }
    }
}