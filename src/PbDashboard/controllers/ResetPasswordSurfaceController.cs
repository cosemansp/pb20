﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.Services.Protocols;
using Umbraco.Web.Mvc;

namespace PbDashboard.controllers
{
    public class ResetPasswordViewModel
    {
        public string Email { get; set; }
        public string Key { get; set; }
        public string Password { get; set; }
        public string PasswordVerify { get; set; }

        public bool PasswordResetted { get; set; }
        public bool IsFailed { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ResetPasswordSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult ResetPassword(string key)
        {
            return PartialView("_passwordReset", new ResetPasswordViewModel { Key = key});
        }

        [HttpPost]
        public ActionResult PostResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                var member = Services.MemberService.GetByEmail(model.Email);

                if (member == null || member.GetValue<string>("activationKey") != model.Key)
                    throw new ValidationException("Invalid password reset or unknown account");

                if (model.Password != model.PasswordVerify)
                    throw new ValidationException("Password confirmation doesn't match password.");

                member.SetValue("activationKey", "");
                Services.MemberService.SavePassword(member, model.Password);
                model.PasswordResetted = true;
            }
            catch (Exception ex)
            {
                model.IsFailed = true;
                model.ErrorMessage = "Something went wrong, this should not happen. Please try again.";
                if (ex is ValidationException)
                    model.ErrorMessage = ex.Message;
            }
            TempData.Add("model", model);
            return CurrentUmbracoPage();
        }
    }
}